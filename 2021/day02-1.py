#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import operator as op

with open('day02-1_input.txt', 'r') as file:
	f = file.readlines()

pos1 = [0, 0]
pos2 = [0, 0]
aim = 0

mv1 = {'forward': [1, 0], 'up': [0, -1], 'down': [0, 1]}

for l in f:
	cmd = l.split()[0].lower()
	val = int(l.split()[1])

	x = [e * val for e in mv1[cmd]]

	pos1 = list(map(op.add, pos1, x))

	hdiff = 0
	ddiff = 0
	if cmd == 'forward':
		pos2 = list(map(op.add, pos2, [val, aim * val]))
	elif cmd == 'up':
		aim -= val
	elif cmd == 'down':
		aim += val

print("Part 1")
print("======")
print(pos1[0] * pos1[1])

print("Part 2")
print("======")
print(pos2[0] * pos2[1])

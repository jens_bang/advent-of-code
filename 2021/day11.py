#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

def ReadFile(filename):
	with open(filename, 'r') as file:
		f = file.read().splitlines()

	l = [list(l) for l in f]
	return [[int(i) for i in x] for x in l]

def PrintOctopi(f):
	for l in f:
		ls = ""
		for o in l:
			ls += f"{o:2} "
		print(ls)
	print("------------------------------")

def EmptyFlashArray(f):
	res = []
	for ol in f:
		nl = []
		for oo in ol:
			nl.append(False)
		res.append(nl)

	return res

def PerformFlashes(f, fa):
	flashes = 0
	for y, l in enumerate(f):
		for x, o in enumerate(l):
			if (o > 9) and not fa[y][x]:
				flashes += 1
				fa[y][x] = True

				for yd in range(-1, 2):
					for xd in range(-1, 2):
						if not((xd == 0) and (yd == 0)):
							if (((y+yd) >= 0) and ((x+xd) >= 0) and ((y+yd) < len(f)) and ((x+xd) < len(f[0]))):
								f[y+yd][x+xd] += 1

	if flashes > 0:
		flashes += PerformFlashes(f, fa)

	return flashes

def PerformStep(f):
	for y, l in enumerate(f):
		for x, o in enumerate(l):
			f[y][x] += 1

	fa = EmptyFlashArray(f)
	res = PerformFlashes(f, fa)

	for y, fl in enumerate(fa):
		for x, fo in enumerate(fl):
			if fo:
				f[y][x] = 0

	return res

#f = ReadFile('day11_test.txt')
f = ReadFile('day11_input.txt')

print("=== Part One ===")

flashes = 0
for x in range(100):
	flashes += PerformStep(f)
print(flashes)

print("=== Part Two ===")

#f = ReadFile('day11_test.txt')
f = ReadFile('day11_input.txt')
ocount = len(f)*len(f[0])
flashes = 0
scount = 0

while flashes != ocount:
	flashes = PerformStep(f)
	scount += 1

print(scount)

#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

class Point:
	x = 0
	y = 0

	def __init__(self, x, y):
		self.x = x
		self.y = y

	def __str__(self):
		return f"({self.x},{self.y})"

class Line:
	f = Point(0,0) ## from
	t = Point(0,0) ## to

	def __init__(self, f, t):
		self.f = f
		self.t = t

	def __str__(self):
		return f"{f} -> {t}"

lines = []

with open('day05-1_input.txt', 'r') as file:
	f = file.read().splitlines()

maxx = 0
maxy = 0

for l in f:
	(b,e) = l.split(' -> ')
	(bx,by) = b.split(',')
	(ex,ey) = e.split(',')
	lines.append(Line(Point(int(bx),int(by)),Point(int(ex),int(ey))))
	if int(bx) > maxx:
		maxx = int(bx)
	if int(ex) > maxx:
		maxx = int(ex)
	if int(by) > maxy:
		maxy = int(by)
	if int(ey) > maxy:
		maxy = int(ey)

points = []

for x in range(maxx + 1):
	points.append([])
	for y in range(maxy + 1):
		points[x].append(0)

for line in lines:
	if (line.f.x == line.t.x) or (line.f.y == line.t.y):
		bx = line.f.x
		by = line.f.y
		ex = line.t.x
		ey = line.t.y
		if bx > ex:
			bx, ex = ex,bx
		if by > ey:
			by,ey = ey,by
		for x in range(bx, ex + 1, 1):
			for y in range(by, ey + 1, 1):
				points[x][y] += 1

count = 0
for x in range(maxx + 1):
	for y in range(maxy + 1):
		if points[x][y] > 1:
			count += 1

print("=== Part One ===")
print(f"{count = }")

for line in lines:
	diffx = abs(line.f.x - line.t.x)
	diffy = abs(line.f.y - line.t.y)
	if (diffx == diffy) and (diffx != 0):
		if line.f.x > line.t.x:
			stepx = -1
		else:
			stepx = 1
		if line.f.y > line.t.y:
			stepy = -1
		else:
			stepy = 1
		curx = line.f.x
		cury = line.f.y
		while (curx != (line.t.x + stepx)) and (cury != (line.t.y + stepy)):
			points[curx][cury] += 1
			curx += stepx
			cury += stepy

count = 0
for x in range(maxx + 1):
	for y in range(maxy + 1):
		if points[x][y] > 1:
			count += 1

print("=== Part Two ===")
print(f"{count = }")

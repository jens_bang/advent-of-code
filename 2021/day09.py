#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

def ReadFile(filename):
	with open(filename, 'r') as file:
		f = file.read().splitlines()

	l = [list(l) for l in f]
	return [[int(i) for i in x] for x in l]

def FindLowPoints(h):
	res = []
	for y, l in enumerate(h):
		for x, p in enumerate(l):
			uh, dh, lh, rh = False, False, False, False
			if x == 0:
				lh = True
			else:
				lh = p < h[y][x-1]
			if y == 0:
				uh = True
			else:
				uh = p < h[y-1][x]
			if x >= len(l) - 1:
				rh = True
			else:
				rh = p < h[y][x+1]
			if y >= len(h) - 1:
				dh = True
			else:
				dh = p < h[y+1][x]
			lowpoint = uh and dh and rh and lh
			if lowpoint:
				res.append(p + 1)

	return res

def FindBasin(h, x, y):
	if x < 0 or y < 0 or x > len(h[0]) - 1 or y > len(h) - 1 or  h[y][x] == 9 or h[y][x] is None:
		res =  0
	else:
		h[y][x] = None
		ub = FindBasin(h, x, y - 1)
		db = FindBasin(h, x, y + 1)
		lb = FindBasin(h, x - 1, y)
		rb = FindBasin(h, x + 1, y)
		res =  ub + db + rb + lb + 1

	return res

def FindBasins(h):
	res = []
	for y, l in enumerate(h):
		for x, p in enumerate(l):
			if p is not None:
				b = FindBasin(h, x, y)

				if b != 0:
					res.append(b)

	return res

#heights = ReadFile('day09_test.txt')
heights = ReadFile('day09_input.txt')

print("=== Part One ===")

print(sum(FindLowPoints(heights)))

print("=== Part Two ===")

basins = FindBasins(heights)
basins = sorted(basins, reverse = True)

b1 = basins.pop(0)
b2 = basins.pop(0)
b3 = basins.pop(0)

print(b1 * b2 * b3)

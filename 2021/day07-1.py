#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from collections import Counter

def CountList(lst):
	return dict(Counter(lst))

def ReadPositions():
	with open('day07-1_input.txt', 'r') as file:
		f = file.read().splitlines()

	#return [int(x) for x in f[0].split(',')]
	ints = [int(x) for x in f[0].split(',')]
	return CountList(ints)

def FuelCost(dist, type):
	dist = abs(dist)
	#print(f"Debug : {dist = }")
	if type == 1:
		return dist
	elif type == 2:
		return ((dist*(dist + 1))/2)
	else:
		return None

def CalcFuelCost(positions, pos, type):
	cost = 0
	for k in positions.keys():
		#print(f"Debug : {pos = }, {k = }, cost = {FuelCost(k - pos, type)}")
		#print(f"Debug : {pos = }, {k = }")
		cost += positions[k] * FuelCost(k - pos, type)

	return cost

def FindCheapestPos(positions, type):
	low = None
	for p in range(min(positions.keys()), max(positions.keys()) + 1):
		cur = CalcFuelCost(positions, p, type)
		if low is None:
			low = cur
		elif cur < low:
			low = cur

	return low

def RunTest():
	test = CountList([16,1,2,0,4,2,7,1,2,14])

	print(test)

	print("=== Test ===")
	print("--- Part One ---")
	print(FindCheapestPos(test, 1))
	print("--- Part Two ---")
	print(FindCheapestPos(test, 2))

	exit(42)


#RunTest()

positions = ReadPositions()

print("=== Part One ===")
print(FindCheapestPos(positions, 1))

print("=== Part Two ===")
print(FindCheapestPos(positions, 2))

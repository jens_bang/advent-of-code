#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from itertools import groupby

def ReadFile(filename):
	with open(filename, 'r') as file:
		f = file.read().splitlines()

	numbers = [int(x) for x in f[0].split(',')]
	f = f[2:]

	boards = [list(sub) for ele, sub in groupby(f, key = bool) if ele]
	boards = [[[int(x) for x in e.split()] for e in i] for i in boards]

	#oneDrawnBoard = [[False, False, False, False, False], [False, False, False, False, False], [False, False, False, False, False], [False, False, False, False, False], [False, False, False, False, False]]

	drawn = []
	for x in range(len(boards)):
		#drawn.append(oneDrawnBoard)
		drawn.append([[False, False, False, False, False], [False, False, False, False, False], [False, False, False, False, False], [False, False, False, False, False], [False, False, False, False, False]])

	return (numbers, boards, drawn)

def DrawNumber(n, boards, drawn):
	for i, b in enumerate(boards):
		for x in range(5):
			for y in range(5):
				if n == b[x][y]:
					drawn[i][x][y] = True

def CalcWinVal(b, d):
	marked = 0
	unmarked = 0

	for x in range(5):
		for y in range(5):
			if d[x][y]:
				marked += b[x][y]
			else:
				unmarked += b[x][y]

	return unmarked

def CheckForWin(boards, drawn, winBoards = None):
	for i, d in enumerate(drawn):
		if winBoards is not None:
			if winBoards[i]:
				continue
		win = False
		for x in range(5):
			winr = d[x][0] and d[x][1] and d[x][2] and d[x][3] and d[x][4]
			winc = d[0][x] and d[1][x] and d[2][x] and d[3][x] and d[4][x]
			win = win or winr or winc
		if win:
			return i

	return None

def RunGame(numbers, boards, drawn):
	for n in numbers:
		DrawNumber(n, boards, drawn)
		win = CheckForWin(boards, drawn)
		if win is not None:
			return n * CalcWinVal(boards[win], drawn[win])

	return None

def FindLastWin(numbers, boards, drawn, winBoards):
	lasti = None
	lastn = None
	for n in numbers:
		DrawNumber(n, boards, drawn)
		i = CheckForWin(boards, drawn, winBoards)
		while i is not None:
			winBoards[i] = True
			#print(f"Debug : {n = :2}, {i = :2}, {winBoards.count(False)}")
			lasti = i
			lastn = n
			if (winBoards.count(False) == 0) or (n == 86):
				print(f"Debug : Exit early")
				return (n, i)
			i = CheckForWin(boards, drawn, winBoards)

	return (lastn, lasti)

def IsFilled(b):
	res = True
	for l in b:
		for n in l:
			if n is not None:
				res = False

	return res

def CheckBoards(numbers, boards):
	allFilled = True
	for b in boards:
		for y, l in enumerate(b):
			for x, n in enumerate(l):
				if n in numbers:
					b[y][x] = None

		allFilled = allFilled and IsFilled(b)

	return allFilled

(numbers, boards, drawn) = ReadFile('day04_test.txt')
#(numbers, boards, drawn) = ReadFile('day04-1_input.txt')

#print(CheckBoards(numbers, boards))

print("=== Part One ===")
print(RunGame(numbers, boards, drawn))

print("=== Part Two ===")
#(numbers, boards, drawn) = ReadFile('day04_test.txt')
(numbers, boards, drawn) = ReadFile('day04-1_input.txt')

winBoards = [False] * len(boards)

(n, i) = FindLastWin(numbers, boards, drawn, winBoards)

if i is not None:
	print(n * CalcWinVal(boards[i], drawn[i]))
else:
	print("None")

#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

def GetMostCommon(bit):
	if bit['0'] > bit['1']:
		return '0'
	else:
		return '1'

def GetLeastCommon(bit):
	if bit['1'] < bit['0']:
		return '1'
	else:
		return '0'

def CalcCommonality(lst, i):
	res = {'0':0, '1':0}
	for l in lst:
		res[l[i]] += 1
	return res

with open('day03-1_input.txt', 'r') as file:
	f = file.read().splitlines()

bitcount = []

for l in f:
	i = 0
	for b in l:
		i += 1
		if i > len(bitcount):
			bitcount.append({'0':0, '1':0})
		bitcount[i-1][b] += 1

gamma = ''
epsilon = ''

oxgen = None
oxgenFilter = ''
co2sc = None
co2scFilter = ''

for b in bitcount:
	gamma += GetMostCommon(b)
	epsilon += GetLeastCommon(b)

oxgenLst = f
i = 0
while len(oxgenLst) > 1:
	oxgenFilter += GetMostCommon(CalcCommonality(oxgenLst, i))
	oxgenLst = [l for l in oxgenLst if l.startswith(oxgenFilter)]
	if len(oxgenLst) == 1:
		oxgen = oxgenLst[0]
	i += 1

co2scLst = f
i = 0
while len(co2scLst) > 1:
	co2scFilter += GetLeastCommon(CalcCommonality(co2scLst, i))
	co2scLst = [l for l in co2scLst if l.startswith(co2scFilter)]
	if len(co2scLst) == 1:
		co2sc = co2scLst[0]
	i += 1

print("== Part One ==")
print(int(gamma,2) * int(epsilon,2))

print("== Part Two ==")
if oxgen is not None and co2sc is not None:
	res2 = int(oxgen,2) * int(co2sc,2)
	print(f"{int(oxgen, 2)} * {int(co2sc, 2)} = {res2}")
else:
	print("Whoops!")

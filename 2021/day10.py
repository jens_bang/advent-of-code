#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

def ReadFile(filename):
	with open(filename, 'r') as file:
		f = file.read().splitlines()

	return f

def StrPop(s):
	res = s[0]
	s = s[1:]

	return (res, s)

def ErrNum(c):
	if c == ')':
		return 3
	elif c == ']':
		return 57
	elif c == '}':
		return 1197
	elif c == '>':
		return 25137
	else:
		return None

def GetCloser(c):
	if c == '(':
		return ')'
	elif c == '[':
		return ']'
	elif c == '{':
		return '}'
	elif c == '<':
		return '>'
	else:
		return None

def CheckLineSyntax(l):
	closers = ''
	if len(l) == 0:
		return (0, l, closers)

	if l[0] in [')', '}', ']', '>']:
		return (ErrNum(l[0]), l, closers)

	(oc, l) = StrPop(l)
	res = 0

	while (len(l) > 0) and (l[0] in ['(', '{', '[', '<']) and (res == 0):
		(res, l, closers) = CheckLineSyntax(l)

	if (len(l) == 0) or (res > 0):
		closers += GetCloser(oc)
		return (res, l, closers) #Incomplete or error

	(fc, l) = StrPop(l)

	if ((oc == '(') and (fc == ')')) or ((oc == '{') and (fc == '}')) or ((oc == '[') and (fc == ']')) or ((oc == '<') and (fc == '>')):
		return (res, l, closers)
	else:
		# There's an error
		return (ErrNum(fc), l, closers)

def CheckSyntaxErrors(f):
	errs = []
	idxs = []
	closers = []
	for i, l in enumerate(f):
		lres = 0
		while (len(l) > 0) and (lres == 0):
			(lres, l, close) = CheckLineSyntax(l)
		if lres > 0:
			errs.append(lres)
			idxs.append(i)
		closers.append(close)

	return (errs, idxs, closers)

def CloserScore(c):
	if c == ')':
		return 1
	elif c == ']':
		return 2
	elif c == '}':
		return 3
	elif c == '>':
		return 4
	else:
		return None

def CalcCloserScores(closers):
	res = []
	for s in closers:
		score = 0
		for c in s:
			score = (score * 5) + CloserScore(c)

		res.append(score)

	return res

#f = ReadFile('day10_test.txt')
f = ReadFile('day10_input.txt')

print("=== Part One ===")

(errs, idxs, closers) = CheckSyntaxErrors(f)
print(sum(errs))

print("=== Part Two ===")

for i in reversed(idxs):
	del closers[i]
	del f[i]

scores = sorted(CalcCloserScores(closers))
middle = int(len(scores) / 2)

print(scores[middle])

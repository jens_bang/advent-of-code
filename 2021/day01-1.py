#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

last = None
inc = 0
dec = 0
tot = 0

with open('day01-1_input.txt', 'r') as f:
	for l in f:
		tot += 1
		i = int(l)
		#print(i)
		if last is not None:
			if i > last:
				inc += 1
			elif i < last:
				dec += 1
		last = i

print(f"Increases      : {inc}")
print(f"Decreases      : {dec}")
print(f"Lines in total : {tot}")

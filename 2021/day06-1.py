#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from collections import Counter

def CountList(lst):
	return dict(Counter(lst))

def ReadTimers():
	with open('day06-1_input.txt', 'r') as file:
		f = file.read().splitlines()

	#return [int(x) for x in f[0].split(',')]
	ints = [int(x) for x in f[0].split(',')]
	return CountList(ints)

def RunSimulation(timers, days, output = False):
	if output:
		print(f"Initial counts     : {timers}")
	for d in range(days):
		new = {}
		for k in range(8,-1,-1):
			if k in timers.keys():
				if k == 0:
					new[6] += timers[k]
					new[8] = timers[k]
				else:
					new[k-1] = timers[k]
			else:
				new[k-1] = 0
		timers = new
		if output:
			print(f"After {d:2} day(s) : {timers}")

	print(f"Fish after {days} days : {sum(timers.values())}")

def RunTest():
	test = CountList([3, 4, 3, 1, 2])
	RunSimulation(test, 18, True)
	test = CountList([3, 4, 3, 1, 2])
	RunSimulation(test,80)

RunTest()

timers = ReadTimers()

print("== Part One ==")
RunSimulation(timers, 80)

print("== Part Two ==")
timers = ReadTimers()

#print(timers)

RunSimulation(timers, 256)

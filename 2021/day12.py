#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

def LoadCaveSystem(filename):
	with open(filename, 'r') as file:
		f = file.read().splitlines()

	l = [list(l) for l in f]
	return [[int(i) for i in x] for x in l]

f = LoadCaveSystem('day12_test-1.txt')
#f = LoadCaveSystem('day12_test-2.txt')
#f = LoadCaveSystem('day12_test-3.txt')
#f = LoadCaveSystem('day12_input.txt')

print("=== Part One ===")

print("42")

print("=== Part Two ===")

print("N/A")
#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

digits = {'abcefg' : 0, 'cf' : 1, 'acdeg' : 2, 'acdfg' : 3, 'bcdf' : 4, 'abdfg' : 5, 'abdefg' : 6, 'acf' : 7, 'abcdefg' : 8, 'abcdfg' : 9}

def ReadFile(filename):
	with open(filename, 'r') as file:
		f = file.read().splitlines()

	#return [int(x) for x in f[0].split(',')]
	codes = [[[''.join(sorted(s)) for s in l.split()] for l in x.split('|')] for x in f]
	return codes

def BySize(words, size):
	return [word for word in words if len(word) == size]

def StrSubtract(a, b):
	if len(a) < len(b):
		(b,a) = (a,b)
	res = ""

	for c in a:
		if c not in b:
			res += c

	return res

def AnalyseLine(hints, output):
	decoder = {}
	encoder = {}

	elements = {'a': '', 'b': '', 'c': '', 'd': '', 'e': '', 'f': '', 'g': ''}

	encoder[1] = BySize(hints, 2)[0]
	decoder[encoder[1]] = 1
	encoder[7] = BySize(hints, 3)[0]
	decoder[encoder[7]] = 7
	encoder[4] = BySize(hints, 4)[0]
	decoder[encoder[4]] = 4
	encoder[8] = BySize(hints, 7)[0]
	decoder[encoder[8]] = 8

	elements['c'] = encoder[1]
	elements['f'] = encoder[1]
	elements['a'] = StrSubtract(encoder[7], encoder[1])
	elements['b'] = StrSubtract(encoder[4], encoder[1])
	elements['d'] = elements['b']
	elements['e'] = StrSubtract(StrSubtract(encoder[8], encoder[4]), encoder[7])
	elements['g'] = elements['e']

	len6 = BySize(h, 6)
	for e in len6:
		diff = StrSubtract(encoder[8],e)
		if diff in encoder[1]:
			encoder[6] = e
			decoder[e] = 6
			elements['c'] = e
			elements['f'] = StrSubtract(elements['f'], e)
		elif diff in encoder[4]:
			encoder[0] = e
			decoder[e] = 0
			elements['d'] = e
			elements['b'] = StrSubtract(elements['b'], e)
		else:
			encoder[9] = e
			decoder[e] = 9
			elements['e'] = e

	len5 = BySize(h, 5)
	for e in len5:
		diff = StrSubtract(encoder[8], e)
		if diff[0] in encoder[4] and diff[1] in encoder[4]:
			encoder[2] = e
			decoder[e] = 2
			# elements
		elif (diff[0] in encoder[1] and diff[1] not in encoder[1]) or (diff[1] in encoder[1] and diff[0] not in encoder[1]):
			encoder[5] = e
			decoder[e] = 5
			# elements
		else:
			encoder[3] = e
			decoder[e] = 3
			#elements

	return (decoder[output[0]] * 1000) + (decoder[output[1]] * 100) + (decoder[output[2]] * 10) + decoder[output[3]]

codes = ReadFile('day08-1_input.txt')
#codes = ReadFile('day08_test.txt')

print("=== Part One ===")

digits = [s for (h, s) in codes]
digits = [i for s in digits for i in s]

lst1 = BySize(digits, 2)
lst4 = BySize(digits, 4)
lst7 = BySize(digits, 3)
lst8 = BySize(digits, 7)

print(len(lst1) + len(lst4) + len(lst7) + len(lst8))

print("=== Part Two ===")

outputs = []
for (h, o) in codes:
	outputs.append(AnalyseLine(h, o))

print(sum(outputs))

#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

def ReadFile(filename):
	with open(filename, 'r') as file:
		f = file.read().splitlines()

	splitter = f.index('')
	pstr = f[:splitter]
	fstr = f[splitter + 1:]

	points = [[int(s) for s in l.split(',')] for l in pstr]

	ftmp = [l[11:] for l in fstr]
	folds = [s.split('=') for s in ftmp]
	for f in folds:
		f[1] = int(f[1])

	return (points, folds)

(p, f) = ReadFile('day13_test.txt')
#f = ReadFile('day13_input.txt')

print("=== Part One ===")

print(f"{p = }")
print(f"{f = }")

print("=== Part Two ===")

print("N/A")
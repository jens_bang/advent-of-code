#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

trimes = []
inc = 0
dec = 0
lin = 0

with open('day01-1_input.txt', 'r') as f:
	for l in f:
		lin += 1

tot = lin
lin = 0

with open('day01-1_input.txt', 'r') as f:
	for l in f:
		lin += 1
		i = int(l)

		enddiff = tot - lin

		if enddiff >= 2:
			trimes.append(i)
			if len(trimes) > 1:
				trimes[-2] += i
			if len(trimes) > 2:
				trimes[-3] += i

			if len(trimes) > 3:
				if trimes[-3] > trimes[-4]:
					inc += 1
				if trimes[-3] < trimes[-4]:
					dec += 1
		else:
			if enddiff >= 1:
				trimes[-2] += i
			if enddiff >= 0:
				trimes[-1] += i

if trimes[-2] > trimes[-3]:
	inc += 1
if trimes[-2] < trimes[-3]:
	dec += 1

if trimes[-1] > trimes[-2]:
	inc += 1
if trimes[-1] < trimes[-2]:
	dec += 1

print(f"Increases      : {inc}")
print(f"Decreases      : {dec}")
print(f"Count of 3-measures : {len(trimes)}")

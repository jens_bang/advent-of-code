# README #

This repository contain my solutions to the Advent of Code. See https://adventofcode.com/ for more info.

### Contribution guidelines ###

These are my solutions. If you have an alternate solution, that's your solution, not mine. ;-)

### Who do I talk to? ###

You can reach me at adventofcode.com@snej.dk if you have any questions.